#!/usr/bin/env python

#######################################################
# File Name : mandBrotDataProc.py
                                                     
#Purpose : Concatonate the output from different PE's
#          and plot the data

#Creation Date : 29-11-2017

#Author :  A.R. Cowie
########################################################

import pandas as pd
import glob
import matplotlib.pyplot as plt
import os

def speedupXcores(dFrame):
    dFrame["Speedup * Cores"] = dFrame["Speedup"]*dFrame["Processing Elements"]
    return dFrame

def serialFrac(dFrame):
    invSpeedup = 1/dFrame["Speedup"]
    invPE = 1/dFrame["Processing Elements"]
    dFrame["Serial Fraction"] = (invSpeedup-invPE)/(1-invPE)

    dFrame.fillna(0, inplace = True)
    #dFrame.iloc[0, "Serial Fraction"] = 0
    return dFrame

def speedup(base, dFrame):
    dFrame["Speedup"] = base/dFrame["Time"]
   # dFrame.iloc[0, "Speedup"] = 0
    return dFrame 


def procDataProb2():

    dFrame1 = pd.read_csv("./hw3Prob2/piCalcTimeStrong.csv", header = None, names = ["Time", "Processing Elements"])
    dFrame1.sort_values(by = "Processing Elements", ascending = True, inplace = True)
    dFrame1 = dFrame1.reindex_axis(["Processing Elements", "Time"], axis = 1)
    dFrame1 = speedup(dFrame1.loc[0]["Time"], dFrame1)
    dFrame1 = serialFrac(dFrame1)

    dFrame2 = pd.read_csv("./hw3Prob2/piCalcTimeWeak.csv", header = None, names = ["Time", "Processing Elements"])
    dFrame2.sort_values(by = "Processing Elements", ascending = True, inplace = True)
    dFrame2 = dFrame2.reindex_axis(["Processing Elements", "Time"], axis = 1)
    dFrame2 = speedup(dFrame2.loc[0]["Time"], dFrame2) 
    dFrame2 = speedupXcores(dFrame2)

    print("Results for Strong Scaling, for Calculating Pi(Problem 2)")
    print(dFrame1)
    
    print("Results for Weak Scaling for Calculating Pi(Problem 2)")
    print(dFrame2) 
    


    plt.figure(1, figsize=(18,17))
    
    sub1 = plt.subplot(1,1,1)
    runtime, = plt.plot(dFrame1["Processing Elements"], dFrame1["Time"], "r")
    sub1.set_ylabel("Run time(sec)")
    sub1.set_xlabel("Processing Elements") 
    ax2 = sub1.twinx()
    spdup, = plt.plot(dFrame1["Processing Elements"], dFrame1["Speedup"], "b")
    ax2.set_ylabel("Speedup")
    plt.title("Strong Scaling and Speed up for calculating Pi(Problem 2)")
    plt.legend([runtime, spdup],["Run time", "Speedup"])
    
    plt.tight_layout() 
    plt.savefig("hw3Prob2StrongScale")

    plt.figure(2, figsize=(18,17))
    sub2 = plt.subplot(2,1,1) 
    runtime, = plt.plot(dFrame2["Processing Elements"], dFrame2["Time"], "r")
    sub2.set_ylabel("Run time(sec)")
    sub2.set_xlabel("Processing Elements") 
    ax3 = sub2.twinx()
    spdup, = plt.plot(dFrame2["Processing Elements"], dFrame2["Speedup"], "b")
    ax3.set_ylabel("Speedup")
    plt.title("Weak Scaling and Speed up for calculating Pi(Problem 2)")
    plt.legend([runtime, spdup],["Run time", "Speedup"])

    sub3 = plt.subplot(2,1,2)
    runtime, = plt.plot(dFrame2["Processing Elements"], dFrame2["Speedup * Cores"], "r")
    sub2.set_ylabel("Speedup * Cores")
    sub2.set_xlabel("Processing Elements") 
    plt.title("Processing Elements vs. Speedup * Cores for calculating Pi(Weak Scaling Problem 2)")
    plt.legend([runtime],["Speedup * Cores"])

    plt.tight_layout()
    plt.savefig("hw3Prob2WeakScale.png")

    plt.show()

def procDataProb1(method1, method2):
    list_1 = []
    list_2 = []
    list_3 = []
    
    files1 = glob.glob("./hw3Prob1/"+method1+"Time*StrongPE"+".csv")

   # files2 = glob.glob("./"+method2+"Time*"+".csv")

    for csvFile in files1:
        dF1 = pd.read_csv(csvFile, names = ["Processing Elements", "Time"])
        list_1.append(dF1)
    
    files2 = glob.glob("./hw3Prob1/"+method2+"Time*"+".csv")

    for csvFile in files2:
        dF2 = pd.read_csv(csvFile, index_col=None, names = ["Processing Elements", "Time"])
        list_2.append(dF2)
    
    files3 = glob.glob("./hw3Prob1/"+method1+"Time*WeakPE"+".csv")
    for csvFile in files3:
        dF3 = pd.read_csv(csvFile, index_col=None, names = ["Processing Elements", "Time", "Points per Processor"])
        list_3.append(dF3)
    

    dFrame1 = pd.concat(list_1)
    dFrame2 = pd.concat(list_2)
    dFrame3 = pd.concat(list_3)
        
    dFrame1.sort_values("Processing Elements", ascending=1, inplace=True)
    dFrame2.sort_values("Processing Elements", ascending=1, inplace=True)
    dFrame3.sort_values("Processing Elements", ascending=1, inplace=True)


    dFrame1 = speedup(dFrame1.iloc[0]["Time"], dFrame1)
    dFrame1 = serialFrac(dFrame1)
    print("Results for: " +method1+ " with Strong Scaling(Problem 1)")
    print(dFrame1)
    
    dFrame2 = speedup(dFrame2.iloc[0]["Time"], dFrame2)
    dFrame2 = serialFrac(dFrame2)
    print("Results for: " +method2+ " with Strong Scaling(Problem 1)")
    print(dFrame2)
    
    dFrame3 = speedup(dFrame3.iloc[0]["Time"], dFrame3)
    dFrame3 = speedupXcores(dFrame3)
    print("Results for: " +method1+ " with Weak Scaling(Problem 1)")
    print(dFrame3)
    
    plt.figure(1, figsize=(18,17))
    sub1 = plt.subplot(2,1,1)
    runtime, = plt.plot(dFrame1["Processing Elements"], dFrame1["Time"], "r")
    sub1.set_ylabel("Run time(sec)")
    sub1.set_xlabel("Processing Elements") 
    ax2 = sub1.twinx()
    spdup, = plt.plot(dFrame1["Processing Elements"], dFrame1["Speedup"], "b")
    ax2.set_ylabel("Speedup")
    plt.title("Strong Scaling and Speed up for " + method1 + " method")
    plt.legend([runtime, spdup],["Run time", "Speedup"])

    sub2 = plt.subplot(2,1,2)
    runtime, = plt.plot(dFrame2["Processing Elements"],dFrame2["Time"], "r")
    sub2.set_ylabel("Run time(sec)")
    sub2.set_xlabel("Processing Elements") 
    ax3 = sub2.twinx()
    spdup, = plt.plot(dFrame2["Processing Elements"], dFrame2["Speedup"], "b")
    ax3.set_ylabel("Speedup")
    plt.title("Strong Scaling and Speed up for " + method2 + " method")
    plt.legend([runtime, spdup],["Run time", "Speedup"])
    
    plt.tight_layout()
    plt.savefig("hw3Prob1StrongScale.png")

    plt.figure(2, figsize=(18,17)) 
    sub3 = plt.subplot(2,1,1) 
    runtime, = plt.plot(dFrame3["Processing Elements"], dFrame3["Time"], "r")
    sub3.set_ylabel("Run time(sec)")
    sub3.set_xlabel("Processing Elements") 
    ax4 = sub3.twinx()
    spdup, = plt.plot(dFrame3["Processing Elements"], dFrame3["Speedup"], "b")
    ax4.set_ylabel("Speedup")
    plt.title("Weak Scaling and Speed up for Grid Method(Problem 1)")
    plt.legend([runtime, spdup],["Run time", "Speedup"])

    sub4 = plt.subplot(2,1,2)
    runtime, = plt.plot(dFrame3["Processing Elements"], dFrame3["Speedup * Cores"], "r")
    sub4.set_ylabel("Speedup * Cores")
    sub4.set_xlabel("Processing Elements") 
    plt.title("Processing Elements vs. Speedup * Cores for Grid Method(Weak Scaling Problem 1)")
    plt.legend([runtime],["Speedup * Cores"])

    plt.tight_layout()
    plt.savefig("hw3Prob1WeakScale.png")
    
    
    plt.show()
    
#def cleanup():
#    files = glob.glob("./*.csv")
 #   for f in files:
  #      os.remove(f)


procDataProb1("grid", "robin")
procDataProb2()
#cleanup()
