#Author: A.R. Cowie
#Description:  My standard template for makefiles
#

#CC = mpiicpc
CC = mpic++
#CC = g++
CFLAGS = -Wall -pedantic -g -I/usr/include/mpich 
LDFLAGS =
DIR = 
PROG = hw4Mandelbrot
DEPS = ChkMPIErr.h mandelLib.h
OBJS = hw4Mandelbrot.o ChkMPIErr.o mandelLib.o


#Project to be built
#

#Build Dependances
%.o: %.cpp $(DEPS)
	$(CC) -c -o $(DIR)$@ $< $(CFLAGS)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(DIR)$@ $^ 
 
clean:
	rm -rf $(DIR)$(OBJS)
 
.PHONY: all clean

