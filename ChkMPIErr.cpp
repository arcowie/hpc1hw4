/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : ChkMPIErr.cpp

* Purpose :  Implementation of functions for handling MPI errors

* Creation Date : 22-11-2017

* Last Modified : Fri 01 Dec 2017 12:45:20 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#include <iostream>
#include "ChkMPIErr.h"
using namespace std;

/**
 * @Description Function handles errors returned by different MPI functions.  The function
 *              evaluates the error and outputs a string identifing the translated error code
 *              as well as the function that caused the error.  At the end the function aborts.
 *
 * @param ierr  Type: int, this is the value returned by the MPI function, and used to determin
 *                    the error class and string.
 * @param errSource Type: int, this corresponds to the error source with the value of the int corresponding
 *                        to the #define string.
 * @param comm Type: MPI_Comm, this is MPI Communicator assoicated with the proc that had an error.
 *
 * @return Type: void
 */

void chkMPIErr(int ierr, int rank, int errSource, MPI_Comm comm){
	if(ierr){
		char errMsg[20];
		int errorClass, lenErrMsg;
		
		MPI_Error_class(ierr, &errorClass);
		MPI_Error_string(errorClass, errMsg, &lenErrMsg);
		cout<<"\nError in Proc Rank: " << rank;
		cout<<"\n\tError source: " << getErrSource(errSource);
		cout<<"\n\tError msg: " << errMsg <<"\n";
		MPI_Abort(comm, ierr);
		MPI_Finalize();

	}
	
}


/**
 * @Description Function takes one of the error source codes and returns the appropriate string.
 *
 * @param getErrSource Type: int, corresponds to the errSource number passed to chkMPIErr.
 *
 * @return Type: string, the string that corresponds to the error source.
 */

string getErrSource(int errSource){
	switch(errSource){
		case 1:   return "MPI_Init";
		case 2:   return "MPI_COMM_rank";
		case 3:   return "MPI_COMM_size";
		case 4:   return "MPI_Send";
		case 5:   return "MPI_Recv";
		case 6:   return "MPI_Finalize";
		case 7:	  return "MPI_Errhandler_set";
		case 8:	  return "MPI_Create_Struct";
		case 9:	  return "MPI_Commit";
		case 10:  return "MPI_Get_address";
		case 11:  return "MPI_Bcast";
		case 12:  return "MPI_Reduce";
		default:  return "Unknown source";
	}
}
