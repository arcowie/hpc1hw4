/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mandelLib.cpp

* Purpose : Library of functions to help with handling the Mandelbrot set

* Creation Date : 01-12-2017

* Last Modified : Sat 02 Dec 2017 09:04:23 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/
#include "mandelLib.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;




string gridSize(int a){
	switch (a){
		case 0:   return "Fine";
		case 1:   return "Medium";
		case 2:   return "Coarse";
		default:  return "Unknown";
	}
}

/**
 * @Description Overloading the (*) opertator for mandPt.  Calculation
 * of Real and imaginary parts uses the formula (a+bi)(c+di)=(ac-bd)+(ad+bc)i
 * In addition the values of .iter and .member from lhs are copied to the temp 
 * and returned with it.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator*(mandPt lhs, mandPt rhs){
	//mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=(lhs.Re*rhs.Re)-(lhs.Im*rhs.Im);
	temp.Im=(lhs.Re*rhs.Im)+(lhs.Im*rhs.Re);
	temp.iter=lhs.iter;
	temp.member=lhs.member;
	return temp;
}


/**
 * @Description Overloading the (+) operator for type mandPt.  In addition to
 * adding the real and imaginary parts the value of the iter and member variables
 * of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */

mandPt operator+(mandPt lhs, mandPt &rhs){
	//mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=lhs.Re+rhs.Re;
	temp.Im=lhs.Im+rhs.Im;
	temp.iter=lhs.iter;
	temp.member=lhs.member;

	return temp;
}


/**
 * @Description Overloading the (+) operator for type mandPt, with the rhs a pointer
 * to a mandPt.  In addition to adding the real and imaginary parts the value of the 
 * iter and member variables of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt*, the right hand side of the op 
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator+(mandPt lhs, mandPt *rhs){
//	mandPt *temp=new mandPt;
	mandPt temp;
	temp.Re=lhs.Re+rhs->Re;
	temp.Im=lhs.Im+rhs->Im;
	temp.iter=lhs.iter;
	temp.member=lhs.member;

	return temp;
}


/**
 * @Description Overloading the (<<) operator(ostream) for the type mandPt, with reference 
 * to the mandPt as the rhs.  This will print out on one line all the members of 
 * the struct and return an ostream object reference for chaining.  Meant for output to
 * the screen.
 *
 * @param os Type: ostream object.
 * @param pt Type: mandPt, the mandPt to be outputed.
 *
 * @return Type: ostream reference for chaining.
 */
ostream& operator<<(ostream &os, mandPt &pt){
	os << "Real: " << pt.Re << " Im: " << pt.Im << " Loops: " << pt.iter << " Member: " << pt.member;
	return os;
}

/**
 * @Description Overloading the (<<) operator(ofstream) for the type mandPt, with reference 
 * to the mandPt as the rhs.  This will print out on one line all the members of 
 * the struct and return an ofstream object reference for chaining.  Meant for output to
 * a file, in a csv format.
 *
 * @param os Type: ostream object.
 * @param pt Type: mandPt, the mandPt to be outputed.
 *
 * @return Type: ostream reference for chaining.
 */
/*ofstream& operator<<(ofstream &os, mandPt &pt){
	os << pt.Re << "," << pt.Im << "," << pt.iter << "," << pt.member;
	return os;
}
*/
/**
 * @Description Checks if the modulus of the complex number indicates that the point
 * will escape to infinity.
 *
 * @param z Type: mandPt, the point being checked
 *
 * @return Type: int, 0 if the point escapes, 1 if it hasn't yet
 */
int modulusCheckerMandPt(const mandPt &z){
	if((((z.Re*z.Re)+(z.Im*z.Im)))>=4){return 0;}
	else{return 1;}
}


/**
 * @Description Checks to see if the point is an elemend of the Mandelbrot set.  This is done
 * by checking the modulos of z after each iteration.  If the modulus of z remains below 2 
 * and the loop gets to the maxIter then the point is in the set and .member is set to 1
 *
 * @param c Type: mandPT, the seed point
 * @param maxIter Type: int, holds the max number of iterations for the loop
 *
 * @return Type: int, return value other than 0 indicates and error
 */
int checkPoint(mandPt *c, const int maxIter){ 
	mandPt z, znext;
	for(int i=0; (i<maxIter)&&modulusCheckerMandPt(z); i++){
		znext=(z*z)+c;
 		c->iter+=1;
		z=znext;
	}
	if(modulusCheckerMandPt(z)){c->member=1;}
	else{c->member=0;}

	return 0;
}


