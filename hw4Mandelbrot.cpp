/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mandelBrot.cpp

* Purpose : Generate the Mandelbrot set using MPI

* Creation Creation Date : 18-11-2017

* Last Modified : Sun 17 Dec 2017 12:26:48 PM EST

* Created By :  Albert R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/
#include <iostream>
#include <mpi.h>
#include "ChkMPIErr.h"
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include "mandelLib.h"
#include <stdlib.h>

using namespace std;


int block(mandInfo *m, int altRank, int size, double &area, int &ptsPerPE){

	int numPtsRe, numPtsIm, numPtsLocal, ierr, temp = 0;
	/* 
	 * realLoopParams[0] is the starting point for the real axis for the PE
	 * realLoopParams[1] is the max value with regards to the real axis for the PE
	 * realLoopParams[2] is the step size with regards to the real axis for the PE
	 */
	 
	double *realLoopParams = new double[3];
 
	//calculate total number of points
	numPtsRe=((m->maxRe-m->minRe)/(size))/m->stepRe;
	numPtsIm=(int)(m->maxIm-m->minIm)/m->stepIm;
	numPtsLocal=numPtsRe*numPtsIm;
	ptsPerPE=numPtsLocal;
	//cout << "numPtsRe(block): " << numPtsRe << " numPtsLocal: " << numPtsLocal << "\n";

	//Array containg the factors for looping through the real part of the complex number
	realLoopParams[0] = m->minRe+(altRank*numPtsRe*m->stepRe);
	realLoopParams[1] = realLoopParams[0]+(numPtsRe*m->stepRe);
	realLoopParams[2] = m->stepRe; 
	
	//Array containg the factors for looping through the imaginary part of the complex number
	


	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsLocal];
	
	//Loops through the points like a matrix a row at at time
	for(double i=m->minIm; i<=m->maxIm;i+=m->stepIm){
		for(double j=realLoopParams[0]; j<=realLoopParams[1]; j+=m->stepRe ){
			
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], m->maxIter);
			if(pointsLocal[temp].member==1){area+=(m->stepIm*m->stepRe);}
			temp++;
		}
	}
	
	cout << "\nPoints per PE: " << numPtsLocal << "\nStep Real: " << m->stepRe << " Step Imaginary: " << m->stepIm << \
		"\nLoop iterations: " << temp << " Points Real: " << numPtsRe << " Points Imaginary: " << numPtsIm <<\
		"\nMax Re: " << m->maxRe << " Min Re: " << m->minRe << " Max Im: " << m->maxIm << " Min Im: " << m->minIm <<\
		"\naltRank: " << altRank;		
	//cout <<"Start loop: " << realLoopParams[0] << " End Loop: " << realLoopParams[1] << "\n";
	//close the output file for this PE, grid size, and method of point selection.
	//delete[] realLoopParams;
	//delete[] pointsLocal;
	return 0;
}

/**
 * @Description This function calculates the Mandelbrot set using a round robin
 * 				technique for determining which points each PE handles
 *
 * @param m Type: struct of mandInfo, contains the information for constructing the grid.
 * @param rank Type: int, the rank of the PE calling the function.
 * @param size Type: int, number of PE total.
 * @param round Type: int, identifies which round of calculations to identify fine, medium, coarse
 * @param area Type: double, the total area of members of the Mandelbrot set for this PE
 * 
 * @return Type: int, and error code, 0 means no error.
 */


int roundRobin(mandInfo *m, int rank, int size, double &areaRobin, int &ptsPerPE){

	int numPtsRe, numPtsIm, numPtsTot, numPtsLocal, ierr, temp = 0;
	

	//calculate total number of points
	numPtsRe=(int)((m->maxRe-m->minRe)/m->stepRe);
	numPtsIm=(int)((m->maxIm-m->minIm)/m->stepIm);
	numPtsTot=numPtsRe*numPtsIm;

	//calculate how many points each PE must evaluate
	if(rank<(numPtsTot%size)){numPtsLocal=(int)((numPtsTot/size)+1);}
	else{numPtsLocal=(int)(numPtsTot/size);}
	
	ptsPerPE = numPtsLocal;
	//creates the array to hold the points dynamically, so stored on the heap
	mandPt *pointsLocal = new mandPt[numPtsLocal];
	
	//Loops through the points like a matrix a row at at time
	for(double i=m->minIm; i<=m->maxIm;i+=m->stepIm){
		for(double j=m->minRe+(rank*m->stepRe); j<m->maxRe; j+=size*m->stepRe ){
			
			pointsLocal[temp].Re=j;
			pointsLocal[temp].Im=i;
			ierr=checkPoint(&pointsLocal[temp], m->maxIter);
	//		cout << "\n" << pointsLocal[temp] << "\n";
			if(pointsLocal[temp].member==1){areaRobin=areaRobin+(m->stepIm*m->stepRe);}
		
			//cout << "\nAREA(robin): " <<area<< " rank: " << rank<<"\n";
			temp++;
		}
	
	}
//	cout << "Rank(robin): " << rank << " area: " << areaRobin << "\n";
//	cout << "numPtsLocal(robin): " << numPtsLocal << "temp " << temp << "\n";

	return 0;
}



/*************START MAIN*****************/
int main(int argc, char **argv){


	int rank, size, ierr, method, scaleType, ptsPerPE;
	double areaBlock = 0 , areaRobin = 0 , areaFinBlock = 0, areaFinRobin = 0, gridSize;
	double robTimeInit = 0, blockTimeInit = 0, robTime = 0, blockTime = 0;
	mandInfo m;
	m.maxRe   = 2.0;
	m.minRe   = -2.0;
	m.maxIm   = 1.0;
	m.minIm   = -1.0;


	
	//assigns default values for the mesh if not provided at the command prompt
	if(argc == 1){
		m.maxIter = 1000;
		m.stepRe  = 0.01;
		m.stepIm  = 0.01;
		method    = 2;
		scaleType = 1;
		cout << "\nNo command line arguments supplied using default\n";
	}

	else if(argc != 6){
		cout << "\n9 arguments are required from the command line\n"\
			"Order of command line Arguments:\n"\
			"<Max Iteration> <Real Axis Step Size> <Imaginary Axis Step Size> <Max Real Value>\n"\
			"<Min real value> <Max Imaginary Value> <Min Imaginary Value> <Method 1 = Round robin, 2 = Grid>\n"\
			 "<Scaling type 1 = Strong 2 = Weak>";
	} 

	else{
		m.maxIter = atof(argv[1]);
		m.stepRe  = atof(argv[2]);
		m.stepIm  = atof(argv[3]);
		method    = atoi(argv[4]);
		scaleType = atoi(argv[5]);
	}

	mandPt setUpPt;
//	MPI_Status status;



/******************Start MPI*******************/

	ierr = MPI_Init(&argc, &argv);chkMPIErr(ierr, rank, MPIInitErr, MPI_COMM_WORLD);
	
	ierr = MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);	

	ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);chkMPIErr(ierr, rank, MPICommRankErr, MPI_COMM_WORLD);
	
	ierr = MPI_Comm_size(MPI_COMM_WORLD, &size);chkMPIErr(ierr, rank, MPICommSizeErr, MPI_COMM_WORLD);
	
	MPI_Comm ALT_COMM;
 
	int color = rank % 2, altRank, altSize;

	MPI_Comm_split(MPI_COMM_WORLD, color, rank, &ALT_COMM);
	
	MPI_Comm_rank(ALT_COMM, &altRank);

	MPI_Comm_size(ALT_COMM, &altSize);
	


	if(color == 0){m.maxRe = 0;}
	if(color == 1){m.minRe = 0 + m.stepRe;}

	if(((m.maxRe-m.minRe)/size)<=m.stepRe*9){cout << "\nError too many PE\n";}
	if(method == 1){
	
		//Call to the roundRobin subroutine for calculating the Mandlebrot set
		robTimeInit = MPI_Wtime();
		ierr = roundRobin(&m, altRank, altSize, areaRobin, ptsPerPE);
		robTimeInit = MPI_Wtime() - robTimeInit;
		if(ierr != 0){cout << "\nError in for loop from roundRobin()\n";}
		
		//MPI_Reduce used to find the max time/the PE that too the most time to calculate its part of the datatset.
		//ierr = MPI_Reduce(&areaRobin, &areaFinRobin, 1 , MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);
		ierr = MPI_Reduce(&robTimeInit, &robTime, 1 , MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, ALT_COMM);
		if(altRank == 0 && rank !=0){
			ierr=MPI_Send(&robTime, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
		}
		else if(rank == 0){
			double temp;

			ierr=MPI_Recv(&temp, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
			robTime = robTime + temp;
		}
	}
	
	else if( method == 2){ 
	blockTimeInit = MPI_Wtime();
	//Call to the block subroutine for calculating the Mandlebrot set
	ierr = block(&m, altRank, altSize, areaBlock, ptsPerPE);
	blockTimeInit = MPI_Wtime() - blockTimeInit;
	if(ierr != 0){cout << "\nError in for loop from block()\n";}
	
	//MPI_Reduce used to find the max time/the PE that too the most time to calculate its part of the datatset.
	//ierr = MPI_Reduce(&areaBlock, &areaFinBlock, 1 , MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);chkMPIErr(ierr, rank, MPIReduceErr, MPI_COMM_WORLD);
	
	ierr = MPI_Reduce(&blockTimeInit, &blockTime, 1 , MPI_DOUBLE, MPI_MAX, 0, ALT_COMM);chkMPIErr(ierr, rank, MPIReduceErr, ALT_COMM);
	
	//Has the altRank 0 PE in ALT_COMM that is not also the rank 0 PE in MPI_WORLD_COMM send the result
	//from its comm's MPI_Reduce
	
	//cout << "\naltRank: " << altRank << " rank: " << rank  << " Color: " << color << "\n" ;
	if((altRank == 0) && (rank !=0)){
		ierr=MPI_Send(&blockTime, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
	}
	else if(rank == 0){
		double temp;

		ierr=MPI_Recv(&temp, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	
		blockTime = blockTime + temp;
	}
	}
	
	

	




	//PE 0 was chosen to receive and write the data to a csv formatted file Prob3SummaryFile.csv.
	if(rank==0){
		
	//	gridSize = m.stepRe*m.stepIm;		
	//	cout << "\nNumber of PE's: " << size << "\n";
	//	cout << "\nGrid: " << gridSize << "\n";
	//	cout << "\nArea Robin: " << areaFinRobin << "\n";
	//	cout << "\nArea Block: " << areaFinBlock << "\n";
	//	cout << "\nRobin Time: " << robTime << "\n";
		//cout << "\nBlock Time: " << blockTime << "\n";
		
		//cout << "before stringstream\n";
		stringstream ss;
		ofstream output;
		string outputFileName;
		//robinTimes.open("testRobin.csv");
		/*cout << "before first ss\n";
		ss << "robinTime" << size << "PE.csv";
		outputFileName = ss.str();
	
		robinTimes.open(outputFileName.c_str());
		*/
		//robinTimes << size << "," << robTime;
		//cout << "in if before 1st file close";
		//robinTimes.close();
		
		//cout << "before clear string\n";
		//ss.str("");
		string methodStr, scaleString;
		try{
			if(method == 1){methodStr = "robinTime";}
			else{methodStr = "gridTime";}
			
			if(scaleType == 1){scaleString = "Strong";}
			else{scaleString = "Weak";}
			
			ss << methodStr << size << scaleString <<"PE.csv";
			outputFileName = ss.str();
	
			//cout << "Before open second file\n";
			//robinTimes.open("testGrid.csv");
			output.open(outputFileName.c_str());
			if(scaleType == 1){output << size << "," << blockTime;}
			else{output << size << "," << blockTime << "," << ptsPerPE;}
			//cout << "in if before 2nd file close";
		}

		catch(exception e){
			cout << "Rank: " << rank << " threw exception: " << e.what() << "\n";
		}
		//cout << "in if after file close";
	}	
	
	//cout << "Just Before finalize ranks: "<< rank << "\n";
	ierr = MPI_Comm_free(&ALT_COMM);

	ierr = MPI_Finalize();chkMPIErr(ierr, rank, MPIFinalErr, MPI_COMM_WORLD);


	return ierr;
}
	
