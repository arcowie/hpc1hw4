/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.

* File Name : mandelLib.h

* Purpose :

* Creation Date : 01-12-2017

* Last Modified : Sat 02 Dec 2017 09:29:01 PM EST

* Created By :  A.R. Cowie

_._._._._._._._._._._._._._._._._._._._._.*/

#ifndef mandelBrot_h
#define mandelBrot_h

#include <iostream>
#include <fstream>
#include <string>

/**
 * @Description Struct that holds the data for a generic Mandelbrot point
 *
 * @param Re Type: Double, holds the value of the real component of the point
 * @param Im Type: Double, holds the value of the imaginary component of the point
 * @param iter Type: Int, holds  the number of iterations until a value of 2 is reached or maxIter
 * @param member Type: Int, holds the value 1 if it is a member of the set and 0 if not
 */
struct mandPt {
	double Re;
	double Im;
	int iter;
	int member;
	mandPt():Re(0.0),
			 Im(0.0),
			 iter(0),
			 member(0){}
	mandPt& operator=(mandPt rhs){
			if(this!=&rhs){
				this->Re=rhs.Re;
				this->Im=rhs.Im;
				this->iter=rhs.iter;
				this->member=rhs.member;
			}
			return *this;
		}

};


/**
 * @Description  Struct containing the information for the grid to eval the points on
 *
 * @param maxRe Type: double, the max value of the Real part of the complex number
 * @param minRe Type: double, the min value of the Real part of the complex number
 * @param maxIm Type: double, the max value of the Imaginary part of the complex number
 * @param minRe Type: double, the min value of the Imaginary part of the complex number
 * @param stepRe Type: double, the step size for the Real part of the complex number
 * @param stepIm Type: double, the step size for the imaginary part of the complex number
 * @param maxiter Type: int, the max number of iterations to see if the point escapes to infinity
 */
 struct mandInfo {
		double maxRe;
	    double minRe;
		double stepRe;	
		double maxIm;
		double minIm;
		double stepIm;
		int maxIter;
};

std::string gridSize(int a);

/**
 * @Description Overloading the (*) opertator for mandPt.  Calculation
 * of Real and imaginary parts uses the formula (a+bi)(c+di)=(ac-bd)+(ad+bc)i
 * In addition the values of .iter and .member from lhs are copied to the temp 
 * and returned with it.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator*(mandPt lhs, mandPt rhs);
	
/**
 * @Description Overloading the (+) operator for type mandPt.  In addition to
 * adding the real and imaginary parts the value of the iter and member variables
 * of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt, the right hand side of the op
 *
 * @return Type: mandPt, returns the result
 */

mandPt operator+(mandPt lhs, mandPt &rhs);

/**
 * @Description Overloading the (+) operator for type mandPt, with the rhs a pointer
 * to a mandPt.  In addition to adding the real and imaginary parts the value of the 
 * iter and member variables of the left hand side are copied.
 *
 * @param lhs Type: mandPt, the left hand side of the op
 * @param rhs Type: mandPt*, the right hand side of the op 
 *
 * @return Type: mandPt, returns the result
 */
mandPt operator+(mandPt lhs, mandPt *rhs);

/**
 * @Description Overloading the (<<) operator(ostream) for the type mandPt, with reference 
 * to the mandPt as the rhs.  This will print out on one line all the members of 
 * the struct and return an ostream object reference for chaining.  Meant for output to
 * the screen.
 *
 * @param os Type: ostream object.
 * @param pt Type: mandPt, the mandPt to be outputed.
 *
 * @return Type: ostream reference for chaining.
 */
std::ostream& operator<<(std::ostream &os, mandPt &pt);

/**
 * @Description Overloading the (<<) operator(ofstream) for the type mandPt, with reference 
 * to the mandPt as the rhs.  This will print out on one line all the members of 
 * the struct and return an ofstream object reference for chaining.  Meant for output to
 * a file, in a csv format.
 *
 * @param os Type: ostream object.
 * @param pt Type: mandPt, the mandPt to be outputed.
 *
 * @return Type: ostream reference for chaining.
 */
/*std::ofstream& operator<<(std::ofstream &os, mandPt &pt);
*/
/**
 * @Description Checks if the modulus of the complex number indicates that the point
 * will escape to infinity.
 *
 * @param z Type: mandPt, the point being checked
 *
 * @return Type: int, 0 if the point escapes, 1 if it hasn't yet
 */
int modulusCheckerMandPt(const mandPt &z);

/**
 * @Description Checks to see if the point is an elemend of the Mandelbrot set.  This is done
 * by checking the modulos of z after each iteration.  If the modulus of z remains below 2 
 * and the loop gets to the maxIter then the point is in the set and .member is set to 1
 *
 * @param c Type: mandPT, the seed point
 * @param maxIter Type: int, holds the max number of iterations for the loop
 *
 * @return Type: int, return value other than 0 indicates and error
 */
int checkPoint(mandPt *c, const int maxIter);

#endif
